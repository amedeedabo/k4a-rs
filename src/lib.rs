#![allow(non_upper_case_globals)]
#![allow(unused_variables)]
extern crate k4a_sys;

use k4a_sys::*;
use std::ffi::CString;
use std::mem::*;
use std::ptr;

pub use self::enums::*;
mod enums;

pub const ResultSucceeded: i32 = 0;
pub const ResultFailed: i32 = 1;

///A wrapper that represent a Kinect device.
/// In the SDK this is a k4a_device_t.
pub struct Device {
    pub idx: u32,
    pub k4a_device: k4a_device_t,
}
impl Device {
    pub fn get_idx(&self) -> u32 {
        self.idx
    }

    pub fn get_k4a_device(&self) -> k4a_device_t {
        self.k4a_device
    }

    fn new(k4a_device: k4a_device_t, idx: u32) -> Self {
        Self { idx, k4a_device }
    }

    /// Potentially open a connection to a device. The connection may fail, or there may be no
    /// devices attached. This Option<> will be changed into a proper Result type soon.
    pub fn open(device_idx: u32) -> Option<Self> {
        unsafe {
            let device_count = k4a_sys::k4a_device_get_installed_count();
            let mut device: k4a_device_t = ptr::null_mut();
            if device_count == 0 {
                println!("No K4A devices found");
                return None;
            }

            if k4a_device_open(device_idx, &mut device) != ResultSucceeded {
                println!("Failed to open device");
                return None;
            }
            if device.is_null() {
                println!("Failed to open device");
                return None;
            }
            Some(Self::new(device, device_idx))
        }
    }

    /// Tries to return the serial number of the device.
    pub fn get_serial(&self) -> Option<String> {
        unsafe {
            let mut serial_number_length: usize = 0;

            if k4a_device_get_serialnum(
                self.get_k4a_device(),
                ptr::null_mut(),
                &mut serial_number_length,
            ) != k4a_buffer_result_t_K4A_BUFFER_RESULT_TOO_SMALL
            {
                println!("{}: Failed to get serial number length", self.get_idx());
                return None;
            }

            let mut serial_number =
                CString::new(vec![1u8; serial_number_length]).expect("Building a cstring failed");
            let serial_number_ptr = serial_number.into_raw();

            if k4a_device_get_serialnum(
                self.get_k4a_device(),
                serial_number_ptr,
                &mut serial_number_length,
            ) != k4a_buffer_result_t_K4A_BUFFER_RESULT_SUCCEEDED
            {
                println!("{}: Failed to get serial number", self.get_idx());
                return None;
            }
            serial_number = CString::from_raw(serial_number_ptr);
            serial_number.into_string().ok()
        }
    }

    ///Returns how many devices are currently connected to the host computer.
    pub fn installed_count() -> u32 {
        unsafe { k4a_device_get_installed_count() }
    }

    /// Gets the device's calibration at a particular depth mode and color resolution.
    /// The calibration is the parameters that define the physical relation of the
    /// color and IR cameras.
    pub fn get_calibration(
        &self,
        depth_mode: DepthMode,
        color_res: ColorResolution,
    ) -> Option<DeviceCalibration> {
        self._get_calibration(depth_mode as i32, color_res as i32)
    }
    /// Gets the device's calibration at a particular depth mode and color resolution.
    /// The calibration is the parameters that define the physical relation of the
    /// color and IR cameras.
    fn _get_calibration(&self, depth_mode: i32, color_res: i32) -> Option<DeviceCalibration> {
        let mut calibration = MaybeUninit::uninit();
        unsafe {
            let res = k4a_device_get_calibration(
                self.get_k4a_device(),
                depth_mode as i32,
                color_res as i32,
                calibration.as_mut_ptr(),
            );
            if res != ResultSucceeded as i32 {
                None
            } else {
                Some(DeviceCalibration(calibration.assume_init()))
            }
        }
    }

    /// Gets the device's calibration for this configuration object.
    /// Only the depth mode and the color resolution affect the calibration,
    /// this is a wrapper method around Device::get_calibration().
    pub fn get_config_calibration(
        &self,
        config: &DeviceConfiguration,
    ) -> Option<DeviceCalibration> {
        self._get_calibration(config.0.depth_mode, config.0.color_resolution)
    }
    /// Starts the device's cameras.
    /// Returns true if the camers successfully started, false otherwise.
    pub fn start_cameras(&self, config: DeviceConfiguration) -> bool {
        let res = unsafe { k4a_device_start_cameras(self.get_k4a_device(), &(config.0)) };
        res == BufferResult::Succeeded as i32
    }

    /// Gets a Capture (a collection of depth, ir, and color images) from the device.
    pub fn get_capture(&self, timeout_in_ms: u32) -> Option<Capture> {
        let mut capture: k4a_capture_t = ptr::null_mut();
        let res = unsafe {
            k4a_device_get_capture(self.get_k4a_device(), &mut capture, timeout_in_ms as i32)
        };
        //Todo: turn a waitresult in our own type of error
        if res != WaitResult::Succeeded as i32 {
            None
        } else {
            Some(Capture(capture))
        }
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe { k4a_device_close(self.k4a_device) }
    }
}

/// A wrapper around the k4a_capture_t type, which contains the information
/// for a collection of images captured at the same time, eg a color and a depth
/// image.
pub struct Capture(pub k4a_capture_t);
impl Capture {
    fn check_null(image: k4a_image_t) -> Option<Image> {
        if image.is_null() {
            None
        } else {
            Some(Image(image))
        }
    }
    /// Get the capture's depth image.
    pub fn get_depth_image(&self) -> Option<Image> {
        Capture::check_null(unsafe { k4a_capture_get_depth_image(self.0) })
    }
    /// Get the capture's color image.
    pub fn get_color_image(&self) -> Option<Image> {
        Capture::check_null(unsafe { k4a_capture_get_color_image(self.0) })
    }
    /// Get the capture's IR image.
    pub fn get_ir_image(&self) -> Option<Image> {
        Capture::check_null(unsafe { k4a_capture_get_ir_image(self.0) })
    }
}

impl Drop for Capture {
    fn drop(&mut self) {
        unsafe { k4a_capture_release(self.0) }
    }
}

///A wrapper around the k4a_image_t type.
pub struct Image(pub k4a_image_t);
impl Image {
    /// Get the image's height in pixels.
    pub fn get_height_pixels(&self) -> usize {
        unsafe { k4a_image_get_height_pixels(self.0) as usize }
    }
    /// Get the image's width in pixels.
    pub fn get_width_pixels(&self) -> usize {
        unsafe { k4a_image_get_width_pixels(self.0) as usize }
    }
    /// Get the image's stride in bytes. The stride is the number of bytes a row takes.
    pub fn get_stride_bytes(&self) -> usize {
        unsafe { k4a_image_get_stride_bytes(self.0) as usize }
    }

    /// Returns a mut pointer to the image's buffer.
    /// Fairly dangerous.
    pub fn get_buffer(&self) -> *mut u8 {
        let buf = unsafe { k4a_image_get_buffer(self.0) };
        if buf.is_null() {
            panic!("Image has null buffer");
        }
        buf
    }

    /// Create an image with a specific width and size.
    pub fn create(format: ImageFormat, width: usize, height: usize) -> Option<Image> {
        if let Some(pixel_size) = format.get_pixel_size() {
            let mut image = MaybeUninit::uninit();
            unsafe {
                let res = k4a_image_create(
                    format as i32,
                    width as i32,
                    height as i32,
                    (width * pixel_size) as i32,
                    image.as_mut_ptr(),
                );
                if res != ResultSucceeded as i32 {
                    None
                } else {
                    Some(Image(image.assume_init()))
                }
            }
        } else {
            None
        }
    }
    /// Create a new image with the same dimensions as this one, with
    /// the given format.
    pub fn create_like(&self, format: ImageFormat) -> Option<Image> {
        Image::create(format, self.get_width_pixels(), self.get_height_pixels())
    }
}
impl Drop for Image {
    fn drop(&mut self) {
        unsafe { k4a_image_release(self.0) }
    }
}

/// Holds information about the configuration of a device.
/// This is a newtype around the k4a_device_configuration_t type.
pub struct DeviceConfiguration(pub k4a_device_configuration_t);
impl DeviceConfiguration {
    ///Creates a new DeviceConfiguration.
    pub fn new(
        color_format: ImageFormat,
        color_resolution: ColorResolution,
        depth_mode: DepthMode,
        camera_fps: CameraFps,
        synchronized_images_only: bool,
        depth_delay_off_color_usec: u32,
        wired_sync_mode: WiredSyncMode,
        subordinate_delay_off_master_usec: u32,
        disable_streaming_indicator: bool,
    ) -> Self {
        Self(k4a_sys::k4a_device_configuration_t {
            color_format: color_format as i32,
            color_resolution: color_resolution as i32,
            depth_mode: depth_mode as i32,
            camera_fps: camera_fps as i32,
            synchronized_images_only,
            depth_delay_off_color_usec: depth_delay_off_color_usec as i32,
            wired_sync_mode: wired_sync_mode as i32,
            subordinate_delay_off_master_usec,
            disable_streaming_indicator,
        })
    }
}
impl Default for DeviceConfiguration {
    fn default() -> Self {
        Self(k4a_sys::k4a_device_configuration_t {
            color_format: ImageFormat::ColorMJPG as i32,
            color_resolution: ColorResolution::On720P as i32,
            depth_mode: DepthMode::WFOV_2X2BINNED as i32,
            camera_fps: CameraFps::Fps30 as i32,
            synchronized_images_only: false,
            depth_delay_off_color_usec: 0,
            wired_sync_mode: WiredSyncMode::Standalone as i32,
            subordinate_delay_off_master_usec: 0,
            disable_streaming_indicator: false,
        })
    }
}

/// Holds information about the calibration of a device.
/// This is a newtype around the k4a_calibration_t type.
pub struct DeviceCalibration(pub k4a_calibration_t);
impl Default for DeviceCalibration {
    fn default() -> Self {
        Self(k4a_calibration_t {
            depth_camera_calibration: CameraCalibration::default().0,
            color_camera_calibration: CameraCalibration::default().0,
            extrinsics: [[CalibrationExtrinsics::default().0; 4usize]; 4usize],
            color_resolution: 0,
            depth_mode: 0,
        })
    }
}

/// Holds information about the calibration of a single camera,
/// For example the color camera on the K4A.
/// This is a newtype around k4a_camera_calibration_t.
pub struct CameraCalibration(k4a_calibration_camera_t);
impl Default for CameraCalibration {
    fn default() -> Self {
        Self(k4a_calibration_camera_t {
            extrinsics: CalibrationExtrinsics::default().0,
            intrinsics: CalibrationIntrinsics::default().0,
            resolution_width: 0,
            resolution_height: 0,
            metric_radius: 0f32,
        })
    }
}

/// Holds information about how a camera is placed in space.
/// This is a newtype around _k4a_calibration_extrinsics_t.
pub struct CalibrationExtrinsics(k4a_calibration_extrinsics_t);
impl Default for CalibrationExtrinsics {
    fn default() -> Self {
        Self(k4a_calibration_extrinsics_t {
            rotation: [0.0; 9],
            translation: [0.0; 3],
        })
    }
}

/// Holds information about camera calibration.
/// This is a newtype around _k4a_calibration_intrinsics_t.
pub struct CalibrationIntrinsics(k4a_calibration_intrinsics_t);
impl Default for CalibrationIntrinsics {
    fn default() -> Self {
        Self(k4a_calibration_intrinsics_t {
            type_: 0,
            parameter_count: 0,
            parameters: CalibrationIntrinsicParameters::default().0,
        })
    }
}
/// Newtype around k4a_calibration_intrinsic_parameters_t
/// Holds Parameters for the CalibrationIntristics.
pub struct CalibrationIntrinsicParameters(k4a_calibration_intrinsic_parameters_t);
impl Default for CalibrationIntrinsicParameters {
    fn default() -> Self {
        Self(k4a_calibration_intrinsic_parameters_t { v: [0.0; 15] })
    }
}

pub fn is_any_nan(f3: &k4a_float3_t__xyz) -> bool {
    f3.x.is_nan() || f3.y.is_nan() || f3.z.is_nan()
}

pub struct Transformation(k4a_transformation_t);

impl Transformation {
    pub fn create(calibration: DeviceCalibration) -> Option<Self> {
        let res = unsafe { k4a_transformation_create(&calibration.0) };
        if res.is_null() {
            None
        } else {
            Some(Transformation(res))
        }
    }

    ///Transforms the depth map into the geometry of the color camera.
    pub fn depth_image_to_color_camera(
        &self,
        depth_image: &Image,
        transformed_depth_image: &mut Image,
    ) -> bool {
        unsafe {
            let res = k4a_transformation_depth_image_to_color_camera(
                self.0,
                depth_image.0,
                transformed_depth_image.0,
            );
            res == ResultSucceeded
        }
    }
}

impl Drop for Transformation {
    fn drop(&mut self) {
        unsafe { k4a_transformation_destroy(self.0) }
    }
}
