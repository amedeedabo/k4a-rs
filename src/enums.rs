#![allow(unused_variables)]
#![allow(dead_code)]

#[repr(i32)]
pub enum BufferResult {
    Succeeded = 0,
    Failure = 1,
    BufferTooSmall = 2,
}

#[repr(i32)]
pub enum WaitResult {
    Succeeded = 0,
    Failure = 1,
    Timeout = 2,
}

/// Verbosity levels of debug messaging
#[repr(i32)]
pub enum LogLevel {
    /// Most severe level of debug messaging.
    Critical = 0,
    /// 2nd most severe level of debug messaging.
    Error = 1,
    /// 3nd most severe level of debug messaging.
    Warning = 2,
    /// 2nd least severe level of debug messaging.
    Info = 3,
    /// Least severe level of debug messaging.
    Trace = 4,
    /// No logging is performed
    Off = 5,
}

/// Depth sensor capture modes.
#[allow(non_camel_case_types)]
#[repr(i32)]
pub enum DepthMode {
    /// Depth sensor will be turned off with this setting.
    OFF = 0,
    /// Depth captured at 320x288. Passive IR is also captured at 320x288.
    NFOV_2X2BINNED = 1,
    /// Depth captured at 640x576. Passive IR is also captured at 576.
    NFOV_UNBINNED = 2,
    /// Depth captured at 512x512. Passive IR is also captured at 512x512.
    WFOV_2X2BINNED = 3,
    /// Depth captured at 1024x1024. Passive IR is also captured at 1024x1024.
    WFOV_UNBINNED = 4,
    /// Passive IR only, captured at 1024x1024.
    PASSIVE_IR = 5,
}

/// Color sensor resolutions.
#[repr(i32)]
pub enum ColorResolution {
    /// Color camera will be turned off with this setting
    Off = 0,
    /// 1280 * 720  16:9
    On720P = 1,
    /// 1920 * 1080 16:9
    On1080P = 2,
    /// 2560 * 1440 16:9
    On1440P = 3,
    /// 2048 * 1536 4:3
    On1536P = 4,
    /// 3840 * 2160 16:9
    On2160P = 5,
    /// 4096 * 3072 4:3
    On3072P = 6,
}

/// Color and depth sensor frame rate.
/// This enumeration is used to select the desired frame rate to operate the cameras. The actual
/// frame rate may vary slightly due to dropped data, synchronization variation between devices,
/// clock accuracy, or if the camera exposure priority mode causes reduced frame rate.
#[repr(i32)]
pub enum CameraFps {
    Fps5 = 0,
    Fps15 = 1,
    Fps30 = 2,
}

/// The image format indicates how the \\ref k4a_image_t buffer data is interpreted.
/// Stride indicates the length of each line in bytes and should be used to determine the start location of each line
/// of the image in memory. Chroma has half as many lines of height and half the width in pixels of the luminance.
/// Each chroma line has the same width in bytes as a luminance line.
#[repr(i32)]
pub enum ImageFormat {
    /// The buffer for each image is encoded as a JPEG and can be decoded by a JPEG decoder.
    /// Because the image is compressed, the stride parameter is not applicable.
    /// Each MJPG encoded image in a stream may be of differing size depending on the compression efficiency.
    ColorMJPG = 0,
    /// NV12 images separate the luminance and chroma data such that all the luminance is at the
    /// beginning of the buffer, and the chroma lines follow immediately after.
    ColorNV12 = 1,
    ///  YUY2 stores chroma and luminance data in interleaved pixels.
    ColorYUY2 = 2,
    ///  Each pixel of BGRA32 data is four bytes. The first three bytes represent Blue, Green,
    ///  and Red data. The fourth byte is the alpha channel and is unused in the Azure Kinect APIs.
    ///  The Azure Kinect device does not natively capture in this format. Requesting images of this format
    ///  requires additional computation in the API.
    ColorBGRA32 = 3,
    ///  Each pixel of DEPTH16 data is two bytes of little endian unsigned depth data. The unit of the data is in
    ///  millimeters from the origin of the camera.
    Depth16 = 4,
    ///  Each pixel of IR16 data is two bytes of little endian unsigned depth data. The value of the data represents
    ///  brightness.
    Ir16 = 5,
    ///  Each pixel of CUSTOM8 is a single channel one byte of unsigned data.
    Custom8 = 6,
    ///  Each pixel of CUSTOM16 is a single channel two bytes of little endian unsigned data.
    Custom16 = 7,
    ///  Custom image format.
    ///  Used in conjunction with user created images or images packing non-standard data.
    Custom = 8,
}
impl ImageFormat {
    /// Returns the number of bytes per row pixel.
    /// The exceptions are:
    /// * MJPG does not have a constant size, as the image is compressed.
    /// * Custom is unknown.
    pub fn get_pixel_size(&self) -> Option<usize> {
        match self {
            ImageFormat::ColorMJPG => None,
            ImageFormat::ColorNV12 => Some(1),
            ImageFormat::ColorYUY2 => Some(2),
            ImageFormat::ColorBGRA32 => Some(4),
            ImageFormat::Depth16 => Some(2),
            ImageFormat::Ir16 => Some(2),
            ImageFormat::Custom8 => Some(1),
            ImageFormat::Custom16 => Some(2),
            ImageFormat::Custom => None,
        }
    }
}

//// Synchronization mode when connecting two or more devices together.
#[repr(i32)]
pub enum WiredSyncMode {
    /// Neither 'Sync In' or 'Sync Out' connections are used."
    Standalone = 0,
    /// The 'Sync Out' jack is enabled and synchronization data it driven out the
    /// connected wire. While in master mode the color camera must be enabled as part of the
    /// multi device sync signalling logic. Even if the color image is not needed, the color
    /// camera must be running.
    Master = 1,
    /// The 'Sync In' jack is used for synchronization and 'Sync Out' is driven for the
    /// next device in the chain. 'Sync Out' is a mirror of 'Sync In' for this mode.
    Subordinate = 2,
}

/// Transformation interpolation type used with k4a_transformation_depth_image_to_color_camera_custom.
#[repr(i32)]
pub enum InterpolationType {
    /// Nearest neighbor interpolation
    Nearest = 0,
    /// Linear interpolation
    Linear = 1,
}

/// Specifies a type of calibration.
#[repr(i32)]
pub enum CalibrationType {
    /// Calibration type is unknown
    Unknown = -1,
    /// Depth sensor
    Depth = 0,
    /// Color sensor
    Color = 1,
    /// Gyroscope sensor
    Gyro = 2,
    /// Accelerometer sensor
    Accel = 3,
    ///Number of types excluding unknown type
    Num = 4,
}
