# k4a-rs

A crate that enables using the Azure Kinect DK camera from Rust.

This is a wrapper around `k4a-sys`, which uses the C FFI to link against
the functions in the Kinect Azure SDK.

It's very incomplete at the moment, and probably changing a lot.
I don't intend to implement all the functionality in the SDK, so if you
find you need something that's not here, jump in and add it, using `k4a-sys`!

## Install

Cargo.toml

```
k4a = "0.1.0"
```

or

```
cargo add k4a
```

## Examples

There are a couple of examples in the `/examples` folder, and you can run them with:

```
cargo run --example enumerate
```

They all require an Azure Kinect to be connected.

## API Design

I've never written a library in Rust before, so constructive feedback about the shape of this
API is welcome. I'm trying to follow convention and implement idiomatic traits.

## Todo

Many of the wrapper structs do not have all their methods implemented (eg Image doesn't support everything `k4a_image_t` does), so their `k4a-sys` raw fields are left public atm, in case one needs to still call k4a-sys.

- [x] k4a enums now have Rust enums
- [ ] enums need a `fromPrimitive` trait, and to look into a way to not have to write `as i32` everywhere
- [ ] Finish porting class methods over, and remove public k4a-sys fields
- [ ] Change functions that return `Option`s into informative `Result`s, with our own `Error` type
