#![allow(non_upper_case_globals)]
extern crate k4a_sys;
extern crate k4a;
use std::ptr;
use std::env;
use std::mem;
use std::fs::File;
use k4a_sys::*;
use k4a::*;
use std::io::Write;

fn create_xy_table(calibration: &mut DeviceCalibration, xy_table: &mut k4a_image_t) {
    unsafe {
        //ATM I think this does a copy, doesn't actually mutate the
        //data so it shouldn't work
        //TODO: Figure out if this works/ is ok
        let mut table_data : &mut [k4a_float2_t] = mem::transmute_copy(&k4a_image_get_buffer(*xy_table));
        let width  = calibration.0.depth_camera_calibration.resolution_width;
        let height = calibration.0.depth_camera_calibration.resolution_height;
        let mut p = k4a_float2_t {
            xy: k4a_float2_t__xy {
                x: 0.0,
                y: 0.0,
            }
        };
        let mut ray = k4a_float3_t {
            xyz: k4a_float3_t__xyz {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            }
        };
        let mut valid = 0;
        let mut idx = 0;
        for y in 0..height {
            p.xy.y = y as f32;
            for x in 0..width {
                p.xy.x = x as f32;
                let _ = k4a_calibration_2d_to_3d(
                    &mut (calibration.0), &p,1.0,
                    k4a_calibration_type_t_K4A_CALIBRATION_TYPE_DEPTH,
                    k4a_calibration_type_t_K4A_CALIBRATION_TYPE_DEPTH,
                    &mut ray, &mut valid);
                    if valid == k4a_result_t_K4A_RESULT_SUCCEEDED {
                        table_data[idx].xy.x = ray.xyz.x;
                        table_data[idx].xy.y = ray.xyz.y;
                    } else {
                        table_data[idx].xy.x = std::f32::NAN;
                        table_data[idx].xy.y = std::f32::NAN;
                    }
                idx += 1;
            }
        }
    }
}
fn generate_point_cloud(depth_image: k4a_image_t, xy_table: k4a_image_t, point_cloud: &mut k4a_image_t, point_count: &mut usize)
{
    unsafe {
        let width = k4a_image_get_width_pixels(depth_image);
        let height = k4a_image_get_height_pixels(depth_image);

        let depth_data: &[u16] = mem::transmute_copy(&k4a_image_get_buffer(depth_image));
        let xy_table_data: &[k4a_float2_t]= mem::transmute_copy(&k4a_image_get_buffer(xy_table));
        let mut point_cloud_data: &mut [k4a_float3_t]= mem::transmute_copy(&k4a_image_get_buffer(*point_cloud));

        *point_count = 0;
        for i in 0..((width * height) as usize) {
            if depth_data[i] != 0 {
                let x = xy_table_data[i].xy.x;
                let y = xy_table_data[i].xy.y;
                if !x.is_nan() && !y.is_nan() {
                    point_cloud_data[i].xyz.x = xy_table_data[i].xy.x * (depth_data[i] as f32);
                    point_cloud_data[i].xyz.y = xy_table_data[i].xy.y * (depth_data[i] as f32);
                    point_cloud_data[i].xyz.z = depth_data[i] as f32;
                    *point_count += 1;
                } else {
                    point_cloud_data[i].xyz.x = std::f32::NAN;
                    point_cloud_data[i].xyz.y = std::f32::NAN;
                    point_cloud_data[i].xyz.z = std::f32::NAN;
                }
            }
        }
    }
}
fn write_point_cloud(filename: &str, point_cloud: &mut k4a_image_t, point_count: usize) -> std::io::Result<()> {
    unsafe {
        let width = k4a_image_get_width_pixels(*point_cloud);
        let height = k4a_image_get_height_pixels(*point_cloud);

        let point_cloud_data: &mut [k4a_float3_t]= mem::transmute_copy(&k4a_image_get_buffer(*point_cloud));
        let mut file = File::create(filename)?;
        write!(file, "ply")?;
        write!(file, "format ascii 1.0")?;
        write!(file, "element vertex {}", point_count)?;
        write!(file, "property float x")?;
        write!(file, "property float y")?;
        write!(file, "property float z")?;
        write!(file, "end_header")?;
        for i in 0.. ((width * height) as usize) {
            let d = point_cloud_data[i];
            let xyz = d.xyz;
            if is_any_nan(&xyz) {
                continue;
            }
            write!(file, "{} {} {}\n", xyz.x, xyz.y, xyz.z)?;
        }
    }
    Ok(())
}
fn main() {
    unsafe {
    let timeout_in_ms = 1000;

    let args = env::args().collect::<Vec<String>>();
    if args.len() < 2 {
        println!("usage: {} <output file> ", args[0]);
        panic!("Missing output file argument");
    }

    let filename: String = args[1].parse().expect("Unable to parse frame count as a number");
    println!("Capturing point cloud to {}", filename);

    let device_wrapped = k4a::Device::open(0).expect("Unable to open device");
    let device = device_wrapped.get_k4a_device();

    let mut config = DeviceConfiguration::default();//_disabled_all();
    config.0.depth_mode = k4a_depth_mode_t_K4A_DEPTH_MODE_WFOV_2X2BINNED;
    config.0.camera_fps = k4a_fps_t_K4A_FRAMES_PER_SECOND_30;

    let mut calibration = DeviceCalibration::default();
    let res = k4a_device_get_calibration(device, config.0.depth_mode, config.0.color_resolution, &mut (calibration.0));
    if res != k4a_buffer_result_t_K4A_BUFFER_RESULT_SUCCEEDED {
        panic!("Failed to get calibration");
    }
    let mut xy_table: k4a_image_t = ptr::null_mut();
    k4a_image_create(k4a_image_format_t_K4A_IMAGE_FORMAT_CUSTOM,
        calibration.0.depth_camera_calibration.resolution_width,
        calibration.0.depth_camera_calibration.resolution_height,
        calibration.0.depth_camera_calibration.resolution_width * (mem::size_of::<k4a_float2_t>() as i32),
        &mut xy_table);

    create_xy_table(&mut calibration,  &mut xy_table);

    let mut point_cloud: k4a_image_t = ptr::null_mut();
    k4a_image_create(k4a_image_format_t_K4A_IMAGE_FORMAT_CUSTOM,
        calibration.0.depth_camera_calibration.resolution_width,
        calibration.0.depth_camera_calibration.resolution_height,
        calibration.0.depth_camera_calibration.resolution_width * (mem::size_of::<k4a_float3_t>() as i32),
        &mut point_cloud);

    let _ = k4a_device_start_cameras(device, &mut (config.0));
    let mut capture: k4a_capture_t = ptr::null_mut();
    match k4a_device_get_capture(device, &mut capture, timeout_in_ms) {
        r if r == k4a_wait_result_t_K4A_WAIT_RESULT_SUCCEEDED => {},
        r if r == k4a_wait_result_t_K4A_WAIT_RESULT_TIMEOUT => { panic!("Timed out waiting for a capture");},
        r if r == k4a_wait_result_t_K4A_WAIT_RESULT_FAILED => { panic!("Failed to read a capture");},
        r => {panic!("Unknown result code {} from device", r)}
    }

    let depth_image = k4a_capture_get_depth_image(capture);
    if depth_image.is_null() {
        panic!("Failed to get depth image from capture");
    }
    let mut point_count: usize = 0;
    generate_point_cloud(depth_image, xy_table, &mut point_cloud, &mut point_count);
    let _ = write_point_cloud(&filename, &mut point_cloud, point_count);

    k4a_image_release(depth_image);
    k4a_capture_release(capture);
    k4a_image_release(xy_table);
    k4a_image_release(point_cloud);
}
}