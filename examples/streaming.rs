extern crate k4a;
use k4a::*;
use std::env;

fn main() {
    let timeout_in_ms = 1000;

    let args = env::args().collect::<Vec<String>>();
    if args.len() < 2 {
        println!("usage: {} FRAMECOUNT", args[0]);
        println!("Capture FRAMECOUNT color and depth frames from the device using the separate get frame APIs");
        panic!("Missing FRAMECOUNT argument");
    }

    let capture_frame_count: i32 = args[1]
        .parse()
        .expect("Unable to parse frame count as a number");
    println!("Capturing {} frames", capture_frame_count);

    let device = Device::open(0).expect("Unable to open device");
    let mut config = DeviceConfiguration::default();
    config.0.color_format = ImageFormat::ColorMJPG as i32;
    config.0.color_resolution = ColorResolution::On2160P as i32;
    config.0.depth_mode = DepthMode::NFOV_UNBINNED as i32;
    config.0.camera_fps = CameraFps::Fps30 as i32;

    if !device.start_cameras(config) {
        panic!("Failed to start device");
    }
    for _ in 0..capture_frame_count {
        let capture = device
            .get_capture(timeout_in_ms)
            .expect("Failed to read a capture");

        if let Some(image) = capture.get_color_image() {
            print!(
                " | Color res:{:4}x{:4} stride:{:5} ",
                image.get_height_pixels(),
                image.get_width_pixels(),
                image.get_stride_bytes()
            );
        } else {
            print!(" | Color None                       ");
        }
        if let Some(image) = capture.get_ir_image() {
            print!(
                " | Ir16 res:{:4}x{:4} stride:{:5} ",
                image.get_height_pixels(),
                image.get_width_pixels(),
                image.get_stride_bytes()
            );
        } else {
            print!(" | Ir16 None                       ");
        }
        if let Some(image) = capture.get_depth_image() {
            println!(
                " | Depth16 res:{:4}x{:4} stride:{:5}",
                image.get_height_pixels(),
                image.get_width_pixels(),
                image.get_stride_bytes()
            );
        } else {
            println!(" | Depth16 None");
        }
    }
}
