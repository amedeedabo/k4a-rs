///Undistort example. Does not compile atm. 
#![allow(non_upper_case_globals)]
extern crate k4a;
extern crate k4a_sys;
use std::env;
use std::mem;
use std::ptr;
// use std::fs::File;
use k4a::*;
use k4a_sys::*;
// use std::io::Write;

pub struct Pinhole {
    pub px: f32,
    pub py: f32,
    pub fx: f32,
    pub fy: f32,
    pub width: usize,
    pub height: usize,
}

// Compute a conservative bounding box on the unit plane in which all the pints have valid projections
pub fn compute_xy_range(
    calibration: &DeviceCalibration,
    camera: &CalibrationType,
    width: usize,
    height: usize,
    x_min: &mut f32,
    x_max: &mut f32,
    y_min: &mut f32,
    y_max: &mut f32,
) {
    // Step outward from the centre point until we find the bounds of validprojection
    let step_u = 0.25;
    let step_v = 0.25;
    let min_u = 0f32;
    let min_v = 0f32;
    let max_u = ((width as i32) - 1) as f32;
    let max_v = ((height as i32) - 1) as f32;
    let center_u = 0.5f32 * (width as f32);
    let center_v = 0.5f32 * (height as f32);

    let mut valid = 0;
    let mut p = k4a_float2_t {
        xy: k4a_float2_t__xy { x: 0.0, y: 0.0 },
    };
    *x_min = find_extreme(
        calibration,
        camera,
        &mut p,
        center_u,
        center_v,
        step_u * -1.0,
        0.0,
    )
    .x;
    *x_max = find_extreme(calibration, camera, &mut p, center_u, center_v, step_u, 0.0).x;
    *y_min = find_extreme(calibration, camera, &mut p, center_u, center_v, 0.0, step_v).y;
    *x_min = find_extreme(
        calibration,
        camera,
        &mut p,
        center_u,
        center_v,
        0.0,
        step_v * -1.0,
    )
    .y;
}
pub fn find_extreme(
    calibration: &k4a_calibration_t,
    camera: &k4a_calibration_type_t,
    p: &mut k4a_float2_t,
    start_u: f32,
    start_v: f32,
    u_step: f32,
    v_step: f32,
) -> k4a_float3_t__xyz {
    let mut u = start_u;
    let mut v = start_v;
    let mut ray = k4a_float3_t {
        xyz: k4a_float3_t__xyz {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        },
    };
    let mut valid: i32 = 0;
    while valid == 0 {
        p.xy.x = u;
        p.xy.y = v;
        unsafe {
            k4a_calibration_2d_to_3d(calibration, p, 1f32, *camera, *camera, &mut ray, &mut valid);
        }
        u += u_step;
        v += v_step;
    }
    unsafe { ray.xyz }
}
impl Pinhole {
    pub fn from_xy_range(calibration: &DeviceCalibration, camera: CalibrationType) -> Self {
        let mut px = 0.0;
        let mut py = 0.0;
        let mut fx = 0.0;
        let mut fy = 0.0;
        let mut width = calibration.0.depth_camera_calibration.resolution_width as usize;
        let mut height = calibration.0.depth_camera_calibration.resolution_height as usize;

        if camera == CalibrationType::Color {
            width = calibration.color_camera_calibration.resolution_width as usize;
            height = calibration.color_camera_calibration.resolution_height as usize;
        }
        let mut x_min = 0.0;
        let mut x_max = 0.0;
        let mut y_min = 0.0;
        let mut y_max = 0.0;
        compute_xy_range(
            calibration,
            &camera,
            width,
            height,
            &mut x_min,
            &mut x_max,
            &mut y_min,
            &mut y_max,
        );
        let fx: f32 = (width as f32) / (x_max - x_min);
        let fy: f32 = (height as f32) / (y_max - y_min);
        let px: f32 = (-x_min * fx) * (width as f32);
        let py: f32 = (-y_min * fy) * (height as f32);

        Self {
            px,
            py,
            fx,
            fy,
            width,
            height,
        }
    }
}

pub struct Coordinate {
    pub x: isize,
    pub y: isize,
    pub weight: [f32; 4],
}

enum Interpolation {
    NearestNeighbor,
    Bilinear,
    BilinearDepth,
}

fn main() {
    let timeout_in_ms = 1000;

    let args = env::args().collect::<Vec<String>>();
    if args.len() < 3 {
        println!("usage: {} <interpolation type> <output file>", args[0]);
        println!("interpolation type: ");
        println!("\t- 0: Nearest Neighbor");
        println!("\t- 1: Bilinear");
        println!("\t- 2: Bilinear with invalidation");
        panic!("Missing parameters");
    }

    let interpolation_type: i32 = args[1].parse::<i32>().expect("Invalid interpolation type");
    let filename: String = args[2]
        .parse()
        .expect("Unable to parse frame count as a number");
    println!("Capturing point cloud to {}", filename);

    let device_wrapped = k4a::Device::open(0).expect("Unable to open device");
    let device = device_wrapped.get_k4a_device();
    let mut config = DeviceConfiguration::default();
    config.0.depth_mode = DepthMode::WFOV_2X2BINNED as i32;
    config.0.camera_fps = CameraFps::Fps30 as i32;

    let calibration = device_wrapped
        .get_calibration(DepthMode::WFOV_2X2BINNED, CameraFps::Fps30)
        .expect("Failed to get calibration");

    let mut pinhole: Pinhole =
        Pinhole::from_xy_range(&(calibration.0), CalibrationType::Depth as i32);
    unsafe {
        k4a_image_create(
            k4a_image_format_t_K4A_IMAGE_FORMAT_CUSTOM,
            ImageFormat::Custom as i32,
            pinhole.width as i32,
            pinhole.height as i32,
            (pinhole.width * mem::size_of::<Coordinate>()) as i32,
            &mut lut,
        );
    }

    // let mut point_cloud: k4a_image_t = ptr::null_mut();
    // k4a_image_create(k4a_image_format_t_K4A_IMAGE_FORMAT_CUSTOM,
    //     calibration.0.depth_camera_calibration.resolution_width,
    //     calibration.0.depth_camera_calibration.resolution_height,
    //     calibration.0.depth_camera_calibration.resolution_width * (mem::size_of::<k4a_float3_t>() as i32),
    //     &mut point_cloud);

    device_wrapped.start_cameras(config);
    let capture = device_wrapped
        .get_capture(timeout_in_ms)
        .expect("Failed to get a capture");
    let depth_image = capture.get_depth_image();
}
