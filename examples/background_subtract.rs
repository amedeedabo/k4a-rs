///This is a much simplified version of the green_screen
/// example in the SDK repository. It does not aim to deal
/// with multiple cameras.
///
#[allow(unused_imports)]
use opencv::prelude::*;
extern crate k4a;
use k4a::*; //Device, DeviceConfiguration, ImageFormat};

use opencv::core;
use opencv::highgui;
use opencv::imgproc;
use opencv::videoio;
use std::ffi::c_void;
use std::mem;

fn run() -> opencv::Result<()> {
    let window = "Green Screen";
    let depth_threshold: u16 = 1000; // default to 1 meter
    highgui::named_window(window, 1)?;
    let device = Device::open(0).expect("Unable to open device.");
    let mut config = DeviceConfiguration::default();
    config.0.color_format = k4a::ImageFormat::ColorBGRA32 as i32;
    config.0.color_resolution = k4a::ColorResolution::On720P as i32;
    config.0.depth_mode = k4a::DepthMode::WFOV_2X2BINNED as i32;
    config.0.camera_fps = k4a::CameraFps::Fps30 as i32;
    config.0.synchronized_images_only = true;
    let calibration = device
        .get_calibration(DepthMode::WFOV_2X2BINNED, ColorResolution::On720P)
        .expect("Unable to get device calibration");
    let transformation = Transformation::create(calibration).unwrap();

    device.start_cameras(config);
    let background_capture = device.get_capture(2000).unwrap();
    let background_color = background_capture.get_color_image().unwrap();
    let background_depth = background_capture.get_depth_image().unwrap();
    let mut background_depth_in_color = background_color.create_like(ImageFormat::Depth16).unwrap();
    let cv_background_color = color_to_opencv(background_color)?;
    transformation.depth_image_to_color_camera(&background_depth, &mut background_depth_in_color);
    let cv_background_depth_t = depth_to_opencv(&background_depth_in_color)?;
    let mut cv_background_depth = Mat::default()?;
    cv_background_depth_t.convert_to(&mut cv_background_depth, core::CV_32F, 1.0, 0.0)?;

    let mut background_depth_temp = Mat::default()?;
    let mut temp1 = Mat::default()?;
    let mut temp2 = Mat::default()?;
    let mut temp3 = Mat::default()?;
    let mut output_image = cv_background_color.clone()?; // allocated outside the loop to avoid re-creating every time
    let zeroes = Mat::zeros(output_image.rows()?, output_image.cols()?, core::CV_8UC4)?.to_mat()?;
    let mut frame_count: u128 = 0;

    loop {
        let capture_opt = device.get_capture(100);
        if capture_opt.is_none() {
            println!("Capture failed, trying again");
            continue;
        }
        frame_count += 1;
        let capture = capture_opt.unwrap();
        let color_image = capture.get_color_image().unwrap();
        let mut depth_in_color = color_image.create_like(ImageFormat::Depth16).unwrap();
        let cv_color_image = color_to_opencv(color_image)?;

        let depth_image = capture.get_depth_image().unwrap();
        let ok = transformation.depth_image_to_color_camera(&depth_image, &mut depth_in_color);
        if !ok {
            println!("Couldn't transform depth image to color");
            continue;
        }
        let cv_depth_in_color_t = depth_to_opencv(&depth_in_color)?;
        let mut cv_depth_in_color = Mat::default()?;
        cv_depth_in_color_t.convert_to(&mut cv_depth_in_color, core::CV_32F, 1.0, 0.0)?;

        let mut within_range = Mat::default()?;
        // core::in_range(&cv_depth_in_color, &1.0, &1000.0, &mut within_range)?;
        let mut close_to_bg = Mat::default()?;
        core::subtract(
            &cv_background_depth,
            &2.0,
            &mut close_to_bg,
            &core::no_array()?,
            core::CV_32F,
        )?;
        let all_ones = Mat::ones_size(cv_depth_in_color.size()?, core::CV_32F)?;
        core::in_range(
            &cv_depth_in_color,
            &all_ones,
            &close_to_bg,
            // &cv_background_depth,
            &mut within_range,
        )?;
        // core::in_range(&cv_depth_in_color, &all_ones2, &close_to_bg, &mut too_close)?;

        let all_ones3 = Mat::ones_size(cv_background_depth.size()?, core::CV_8U)?;
        //every 1000 frames, re-set to use the current depth one;
        let (alpha, beta) = if frame_count % 1000 != 0 {
            (0.05, 0.95)
        } else {
            // (0.9, 0.1)
            (0.05, 0.95)
        };
        core::multiply(&cv_depth_in_color, &alpha, &mut temp1, 1.0, core::CV_32F)?;
        core::multiply(&cv_background_depth, &beta, &mut temp2, 1.0, core::CV_32F)?;
        core::add(
            &temp1,
            &temp2,
            &mut cv_background_depth,
            &core::no_array()?,
            core::CV_32F,
        )?;
        zeroes.copy_to(&mut output_image)?;
        // cv_background_color.copy_to(&mut output_image);
        // cv_background_depth.copy_to(&mut output_image);
        cv_color_image.copy_to_masked(&mut output_image, &within_range);
        // hide the rest with the background image
        highgui::imshow(window, &output_image);
        if highgui::wait_key(10)? > 0 {
            break;
        }
    }
    Ok(())
}

fn main() {
    run().unwrap()
}

pub fn color_to_opencv(mut image: Image) -> opencv::Result<Mat> {
    let with_alpha = unsafe {
        let stride = image.get_stride_bytes();
        Mat::new_rows_cols_with_data(
            image.get_height_pixels() as i32,
            image.get_width_pixels() as i32,
            core::CV_8UC4,
            &mut *(image.get_buffer() as *mut c_void),
            stride,
        )?
    };
    let mut no_alpha = Mat::default()?;
    imgproc::cvt_color(&with_alpha, &mut no_alpha, imgproc::COLOR_BGRA2BGR, 0)?;
    return Ok(no_alpha);
}

pub fn depth_to_opencv(image: &Image) -> opencv::Result<Mat> {
    unsafe {
        let stride = image.get_stride_bytes();
        let mat = Mat::new_rows_cols_with_data(
            image.get_height_pixels() as i32,
            image.get_width_pixels() as i32,
            core::CV_16U,
            &mut *(image.get_buffer() as *mut c_void),
            stride,
        );
        mat
    }
}
