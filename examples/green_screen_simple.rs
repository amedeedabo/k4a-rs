///This is a much simplified version of the green_screen example in
///the SDK repository. It does not aim to deal with multiple cameras.
extern crate k4a;
use k4a::*;

<<<<<<< HEAD
use opencv::prelude::*;
use opencv::{core, highgui, imgproc};
use std::ffi::c_void;

fn run() -> opencv::Result<()> {
    let window = "Green Screen";
    let depth_threshold = 1000.0; // default to 1 meter
    highgui::named_window(window, 1)?;
    let device = Device::open(0).expect("Unable to open device.");
    let config = DeviceConfiguration::new(
        ImageFormat::ColorBGRA32,
        ColorResolution::On720P,
        DepthMode::WFOV_2X2BINNED,
        CameraFps::Fps30,
        true,
        0,
        WiredSyncMode::Standalone,
        0,
        false,
    );
    let calibration = device
        .get_config_calibration(&config)
        .expect("Unable to get device calibration");
    let transformation = Transformation::create(calibration).unwrap();

    device.start_cameras(config);
    let background_capture = device.get_capture(2000).unwrap();
    let background_color = background_capture.get_color_image().unwrap();
    let cv_background_color = color_to_opencv(background_color)?;

    let mut output_image = cv_background_color.clone()?; // allocated outside the loop to avoid re-creating every time
    let zeroes = Mat::zeros(output_image.rows()?, output_image.cols()?, core::CV_8UC4)?.to_mat()?;

    loop {
        let capture_opt = device.get_capture(100);
        if capture_opt.is_none() {
            println!("Capture failed, trying again");
            continue;
        }
        let capture = capture_opt.unwrap();
        let color_image = capture.get_color_image().unwrap();
        let mut depth_in_color = color_image.create_like(ImageFormat::Depth16).unwrap();
        let cv_color_image = color_to_opencv(color_image)?;

        let depth_image = capture.get_depth_image().unwrap();
        let ok = transformation.depth_image_to_color_camera(&depth_image, &mut depth_in_color);
        if !ok {
            println!("Couldn't transform depth image to color");
            continue;
        }
        let cv_depth_in_color = depth_to_opencv(&depth_in_color)?;

        let mut within_range = Mat::default()?;
        core::in_range(
            &cv_depth_in_color,
            &1.0,
            &depth_threshold,
            &mut within_range,
        )?;
        zeroes.copy_to(&mut output_image)?;
        cv_color_image.copy_to_masked(&mut output_image, &within_range)?;
        highgui::imshow(window, &output_image)?;
        if highgui::wait_key(10)? > 0 {
            break;
        }
    }
    Ok(())
}

fn main() {
    run().unwrap()
}

pub fn color_to_opencv(image: Image) -> opencv::Result<Mat> {
    let with_alpha = unsafe {
        let stride = image.get_stride_bytes();
        Mat::new_rows_cols_with_data(
            image.get_height_pixels() as i32,
            image.get_width_pixels() as i32,
            core::CV_8UC4,
            &mut *(image.get_buffer() as *mut c_void),
            stride,
        )?
    };
    let mut no_alpha = Mat::default()?;
    imgproc::cvt_color(&with_alpha, &mut no_alpha, imgproc::COLOR_BGRA2BGR, 0)?;
    return Ok(no_alpha);
}

pub fn depth_to_opencv(image: &Image) -> opencv::Result<Mat> {
    unsafe {
        let stride = image.get_stride_bytes();
        let mat = Mat::new_rows_cols_with_data(
            image.get_height_pixels() as i32,
            image.get_width_pixels() as i32,
            core::CV_16U,
            &mut *(image.get_buffer() as *mut c_void),
            stride,
        );
        mat
    }
}
