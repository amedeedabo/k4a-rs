extern crate k4a;
use k4a::*;

fn main() {
    let num_devices = Device::installed_count();
    println!("Found {} connected devices", num_devices);
    for i in 0..num_devices {
        let device = Device::open(i).expect(&format!("Unable to open device {}", i));
        let calibration = device
            .get_calibration(DepthMode::NFOV_UNBINNED, ColorResolution::On1080P)
            .expect("Unable to get device calibration");

        let calib = calibration.0.depth_camera_calibration;

        println!(
            "===== Device {}: {}",
            i,
            device.get_serial().unwrap_or("'No Serial Number'".into())
        );
        println!("resolution width: {} ", calib.resolution_width);
        println!("resolution height: {}", calib.resolution_height);
        unsafe {
            //unsafe because union access
            println!(
                "principal point x: {}",
                calib.intrinsics.parameters.param.cx
            );
            println!(
                "principal point y: {}",
                calib.intrinsics.parameters.param.cy
            );
            println!("focal length x: {}", calib.intrinsics.parameters.param.fx);
            println!("focal length y: {}", calib.intrinsics.parameters.param.fy);
            println!("radial distortion coefficients:");
            println!("k1: {}", calib.intrinsics.parameters.param.k1);
            println!("k2: {}", calib.intrinsics.parameters.param.k2);
            println!("k3: {}", calib.intrinsics.parameters.param.k3);
            println!("k4: {}", calib.intrinsics.parameters.param.k4);
            println!("k5: {}", calib.intrinsics.parameters.param.k5);
            println!("k6: {}", calib.intrinsics.parameters.param.k6);
            println!(
                "center of distortion in Z=1 plane, x: {}",
                calib.intrinsics.parameters.param.codx
            );
            println!(
                "center of distortion in Z=1 plane, y: {}",
                calib.intrinsics.parameters.param.cody
            );
            println!(
                "tangential distortion coefficient x: {}",
                calib.intrinsics.parameters.param.p1
            );
            println!(
                "tangential distortion coefficient y: {}",
                calib.intrinsics.parameters.param.p2
            );
            println!(
                "metric radius: {}",
                calib.intrinsics.parameters.param.metric_radius
            );
        }
    }
}
